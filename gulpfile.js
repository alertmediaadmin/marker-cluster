var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var gutil = require('gulp-util');

gulp.task('compress', function() {
    return gulp.src('marker-cluster.js')
    .pipe(rename('marker-cluster.min.js'))
    .pipe(uglify().on('error', gutil.log))
    .pipe(gulp.dest(''));
});